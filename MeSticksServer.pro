CONFIG += c++11
QT += sql widgets network

HEADERS += \
    dbwork.h \
    server.h \
    client.h

SOURCES += \
    dbwork.cpp \
    server.cpp \
    main.cpp \
    client.cpp
