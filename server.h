#ifndef SERVER_H
#define SERVER_H

#include <client.h>
#include <dbwork.h>

#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <QtCore/QDataStream>

#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

class Server : public QObject
{
    Q_OBJECT
public:
    Server();
    QTcpServer *server;
    QList <QTcpSocket*> listTrySocket;
    QList <QTcpSocket*> listConnectSocket;
    QList <Client*> client;
    DBWork *dbConnect;

    void createServer();

public slots:
    void slotNewConnect();
    void slotUpdateConnection();
};

#endif // SERVER_H
