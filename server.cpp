#include "server.h"

Server::Server(){
    client.clear();
    listTrySocket.clear();
    listConnectSocket.clear();

    createServer();
    dbConnect = new DBWork;
}

void Server::createServer(){
    server = new QTcpServer(this);
    server->listen(QHostAddress::Any, 41993);
    qDebug() << "server create at " << QDateTime::currentDateTime().toString();
    QObject::connect(server, SIGNAL(newConnection()), SLOT(slotNewConnect()));
    QTimer::singleShot(20*1000, this, SLOT(slotUpdateConnection()));
}

void Server::slotNewConnect(){
    client.push_back(new Client(server->nextPendingConnection(), dbConnect));
    qDebug() << "CONNECT!";
}

void Server::slotUpdateConnection()
{
    QTimer::singleShot(20*1000, this, SLOT(slotUpdateConnection()));
    foreach (Client *c, client) {
        if (c->isPing){
            c->startPing = QTime::currentTime();
            c->isPing = false;
            c->stream << (QString)"pingS";
        }
        else {
            delete c;
            client.removeOne(c);
        }
        qDebug() << c->pingTime << client.length();
    }
}


