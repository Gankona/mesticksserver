#ifndef CLIENT_H
#define CLIENT_H

#include <dbwork.h>

#include <QtNetwork/QTcpSocket>

#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QTime>

class Client : public QObject
{
    Q_OBJECT
public:
    Client(QTcpSocket *socketSend,
           DBWork *dataBase);
    QTcpSocket *socket;
    QDataStream stream;
    DBWork *db;
    QTime startPing;

    bool isPing;
    bool isCover;
    int pingTime;

public slots:
    void slotReadyRead();
};

#endif // CLIENT_H
