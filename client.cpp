#include "client.h"

Client::Client(QTcpSocket *socketSend, DBWork *dataBase)
{
    isCover = false;
    db = dataBase;
    socket = socketSend;

    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    stream.setDevice(socket);
    isPing = true;
}

void Client::slotReadyRead()
{
    QString varQuery;
    stream >> varQuery;

    if (varQuery == "login"){
        QString login;
        QString password;
        QStringList list;
        list.clear();
        stream >> login >> password;
        qDebug() << login << password;
        db->query->exec("SELECT `name`,`password`,`email` FROM `Users` WHERE `login` = '"
                       + login + "';");
        db->query->next();
        qDebug() << db->query->value(0).toString()
                 << db->query->value(1).toString();

        if (db->query->value(1).toString() == password){
            qDebug() << "password ==";
            QString queryNew =  "INSERT INTO Users "
                                "(name, login, email, password) VALUES('"
                                + db->query->value(0).toString() + "', '"
                                + login + "', '"
                                + db->query->value(2).toString() + "', '"
                                + password + "');";
            QString queryUpdate = "UPDATE Users SET password = '"
                    + db->query->value(1).toString() + "' WHERE login = '"
                    + login + "';";
            list << db->query->value(0).toString() << login << "" << queryNew << queryUpdate;
        }

        else{
            qDebug() << "password no ==";
            list << "" << "" << "Пароли не совпадают";
        }

        stream << (QString)"login" << list;
    }

    else if(varQuery == "registration"){
        QStringList avtorList;
        avtorList.clear();
        stream >> avtorList;
        QString login = avtorList.at(1);

        bool isLoginPresent = true;
        db->query->exec("SELECT id FROM Users WHERE login = '"
                        + login + "';");
        db->query->next();

        if (db->query->isNull(0))
            isLoginPresent = false;

        qDebug() << varQuery << avtorList;
        QString queryStr = "INSERT INTO Users "
                "(name, login, email, password) VALUES('"
                + avtorList.at(0) + "', '"
                + avtorList.at(1) + "', '"
                + avtorList.at(2) + "', '"
                + avtorList.at(3) + "');";

        bool isWright = db->query->exec(queryStr);
        if (isLoginPresent){
            avtorList.clear();
            avtorList << "Логин уже занят";
        }

        else if (isWright){
            db->query->exec("CREATE TABLE "
                        + login
                        + " (id INTEGER PRIMARY KEY AUTOINCREMENT,"
                        "type INTEGER DEFAULT (0), title STRING,"
                        "image STRING, list INTEGER DEFAULT (- 1),"
                        "text TEXT, timeEnd DATETIME, timeStart DATETIME);");
            db->query->exec("CREATE TABLE "
                        + login
                        + "List (\"group\" INTEGER, title STRING, text STRING,"
                        "isComplete BOOLEAN DEFAULT FALSE, timeEnd DATETIME);");
            avtorList.clear();
            avtorList << "successful";
            avtorList << queryStr;
            avtorList << login;
        }

        else {
            avtorList.clear();
            avtorList << "Неизвестная ошибка";
        }

        stream << (QString)"registration" << avtorList;
    }

    else if(varQuery == "changePassword"){
        QStringList changePass;
        stream >> changePass;
        bool isCorrect = db->query->exec("SELECT password FROM Users "
                                            "WHERE login = '"+changePass.at(0)+"';");
        qDebug() << "SELECT password FROM Users WHERE login = '"+changePass.at(0)+"';"
                 << isCorrect;
        db->query->next();

        if (db->query->value(0).toString() == changePass.at(1)){
            QString query = "UPDATE Users SET password = '"
                    + changePass.at(2)+"' WHERE login = '"+changePass.at(0)+"';";
            bool isCorrect2 = db->query->exec(query);
            qDebug() << query << isCorrect2;
            stream << (QString)"changePassword" << (QStringList{query, "Пароль успешно изменен"});
        }

        else
            stream << (QString)"changePassword" << (QStringList{"", "Пароль не верен"});
    }

    else if (varQuery == "pingS"){
        isPing = true;
        pingTime = startPing.msecsTo(QTime::currentTime());
    }

    else if (varQuery == "pingC"){
        stream << (QString)"pingC";
    }
}
