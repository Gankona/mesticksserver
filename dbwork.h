#ifndef DBWORK_H
#define DBWORK_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtCore/QObject>

class DBWork
{
public:
    DBWork();
    QSqlDatabase db;
    QSqlQuery *query;

    bool connectDB();
};

#endif // DBWORK_H
