#include "dbwork.h"
#include <QDebug>
#include <QSqlError>

DBWork::DBWork(){
    if (! connectDB()){
        qDebug() << "Sorry, we can't connect to DataBase";
    }
    else {
        qDebug() << "Connect to DataBase";
    }
    query = new QSqlQuery(db);
}

bool DBWork::connectDB(){
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("DataBase/MeSticks");
    bool ok = db.open();
    return ok;
}
